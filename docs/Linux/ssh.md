# Protocole SSH

SSH est un protocole de connexion (dont l'abréviation est **Secure SHell**), permettant celle-ci d'une machine hôte à une machine distante. **SSH utilise le port 22.** Deux moyens de connection sont possibles :

* **Avec l'identifiant et le mot de passe** d'un serveur distant. Il est fortement déconseillé d'utiliser le compte root. La commande est ```ssh identifiant@serveur```.
* **Avec la génération d'une clef privée et d'un clef publique** sur la machine hôte, et la fourniture de la clef publique au serveur distant. Cette méthode est plus sécurisée et permet d'éviter de devoir rentrer un mot de passe à chaque confection.
	Pour generer une clef SSH (sur mac), la commande est la suivante : ```ssh-keygen -t rsa "mon@email```.

Si un message d'erreur ***"WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!"*** apparait, il s'agit d'un message indiquant que nos clefs SSH on changées. Il faut donc mettre à jour le fichier **"known_hosts"**, avec la commande ```ssh-keygen -R ip-du-serveur```.

# Tunnel SSH

Un tunnel SSH permet à un ordinateur d'utiliser la connexion d'un serveur distant pour communiquer. Cette communication utilisant le protocole SSH, lui permet d'être chiffrée.