# Cgroups

Cgroups (control groups) est une fonctionnalité du noyau Linux pour limiter, compter et isoler l'utilisation des ressources (processeur, mémoire, utilisation disque, etc.).

![Schéma CGroups](/img/schéma-cgroups.png)