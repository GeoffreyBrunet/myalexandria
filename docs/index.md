# Ce site
Ce site est fait avec MKDocs et utilise le thème Read The Docs. Il vise à rassembler mes connaissances et les mettre à disposition de chacun. La totalité du site (et son infrastructure inclus) est fabriquée à partir de logiciels Open Source. N'hésitez pas à ouvrir une issue sur son dossier Github pour apporter une modification, un complément ou une correction.
## Moi 
* Mon [Github](https://github.com/GeoffreyBrunet).
* Mon [Twitter](https://twitter.com/geoffreybrunet5).
* Mon [Linkedin](https://www.linkedin.com/in/geoffrey-brunet-558315ba/).
## Markdown
* *Italique*
* **Gras**
* **_Gras et italique_**
* ```Code```
* ~~Barré~~
* > Citation
* [Lien URL]()
* Email <email@email.com>
* Image : ![Image](https://myalexandriabucket.s3.fr-par.scw.cloud/img/cern-lhc.jpg)