# Réinitialisation routeur Cisco

> Démarrer le routeur et envoyer un « control break »  
> ```Confreg 0x2142```  
> ```Delete startup-config```  
> Redémarrer et se connecter  
> ```Configure terminal```  
> ```Config-register 0x2102```  
