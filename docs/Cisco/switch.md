# Réinitialisation switch Cisco

> Démarrer le switch en appuyant sur le bouton mode.  
> ```Flash_init```  
> ```Dir flash :```  
> ```Delete flash :config.text```  
> ```Delete flash :vlan.dat```  
> ```reset```  
