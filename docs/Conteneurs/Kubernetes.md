![Kubernetes](https://myalexandriabucket.s3.fr-par.scw.cloud/img/logo-kubernetes.png)
# Fonctionnement
![schéma Kubernetes](https://myalexandriabucket.s3.fr-par.scw.cloud/img/schema-kubernetes.png)
## Node Master
Le node Master  est un ensemble de trois processus qui s’exécutent sur un seul nœud de votre cluster, désigné comme nœud maître (master node en anglais). Ces processus sont: kube-apiserver kube-controller-manager et kube-scheduler.  

> **Etcd** : est une unité de stockage distribuée persistante et légère de données clé-valeur développée par CoreOS, qui permet de stocker de manière fiable les données de configuration du cluster, représentant l'état du cluster à n'importe quel instant. D'autres composants scrutent les changements dans ce stockage pour aller eux-mêmes vers l'état désiré.

> **Serveur d'API** : Le serveur d'API est un élément clé et sert l'API Kubernetes grâce à JSON via HTTP. Il fournit l'interface interne et externe de Kubernetes. Le serveur d'API gère et valide des requêtes REST et met à jour l'état des objets de l'API dans etcd, permettant ainsi aux clients de configurer la charge de travail et les containers sur les nœuds de travail.  

> **L'ordonnanceur** : L'ordonnanceur est un composant additionnel permettant de sélectionner quel node devrait faire tourner un pod non ordonnancé en se basant sur la disponibilité des ressources. L'ordonnanceur gère l'utilisation des ressources sur chaque node afin de s'assurer que la charge de travail n'est pas en excès par rapport aux ressources disponibles. Pour accomplir cet objectif, l'ordonnanceur doit connaître les ressources disponibles et celles actuellement assignées sur les serveurs.  

> **Controller manager** : Le gestionnaire de contrôle (controller manager) est le processus dans lequel s'exécutent les contrôleurs principaux de Kubernetes tels que DaemonSet Controller et le Replication Controller. Les contrôleurs communiquent avec le serveur d'API pour créer, mettre à jour et effacer les ressources qu'ils gèrent (pods, service endpoints, etc.).  

## Node Worker

Chaque nœud non maître d'un cluster exécute deux processus: kubelet qui communique avec le Kubernetes master et kube-proxy, un proxy réseau reflétant les services réseau Kubernetes sur chaque nœud.

> **Kubelet** : Kubelet est responsable de l'état d'exécution de chaque nœud (c'est-à-dire, d'assurer que tous les conteneurs sur un nœud sont en bonne santé). Il prend en charge le démarrage, l'arrêt, et la maintenance des conteneurs d'applications (organisés en pods) dirigé par le plan de contrôle. Kubelet surveille l'état d'un pod et s'il n'est pas dans l'état voulu, le pod sera redéployé sur le même node. Le statut du node est relayé à intervalle de quelques secondes via messages d’état vers le maître. Dès que le maître détecte un défaut sur un node, le Replication Controller voit ce changement d'état et lance les pods sur d'autres hôtes en bonne santé.  

> **Kube-proxy** :  Le kube-proxy est l’implémentation d'un proxy réseau et d'un répartiteur de charge, il gère le service d'abstraction ainsi que d'autres opérations réseaux. Il est responsable d'effectuer le routage du trafic vers le conteneur approprié en se basant sur l'adresse IP et le numéro de port de la requête entrante.  

> **cAdvisor** : cAdvisor est un agent qui surveille et récupère les données de consommation des ressources et des performances comme le processeur, la mémoire, ainsi que l'utilisation disque et réseau des conteneurs de chaque node.  

> **Container Runtime** : Logiciel capable de créer et faire fonctionner les conteuneurs.  

## Context

Le contexte  permet à la commande kubectl de définir le cluster cible, et l'utilsateur (avec ses identifiants). ```kubectl config view``` permet de voir le ou les contextes actuel sur la machine hôte.

## RBAC

> Le contrôle d'accès basé sur les rôles (Role-based access control, RBAC) est une méthode de régulation de l'accès aux ressources informatiques ou de réseau basée sur les rôles des utilisateurs individuels au sein de votre organisation.

> Pour gérer RBAC dans Kubernetes, nous devons déclarer :

* Role et ClusterRole : ce sont un ensemble de règles représentant un ensemble d’autorisations. Un Role ne peut être utilisé que pour accorder l’accès à des ressources dans des namespaces. Un ClusterRole peut être utilisé pour octroyer les mêmes autorisations qu’un rôle, mais également pour octroyer un accès à des ressources à l’échelle du cluster, des endpoints autres que des ressources.
* Subjects : un sujet est l’entité qui effectuera les opérations dans le cluster. Ils peuvent être des comptes d’utilisateurs, des comptes de services ou même un groupe.
* RoleBinding et ClusterRoleBinding : comme son nom l’indique, il ne s’agit que de la liaison entre un sujet et un Role ou un ClusterRole.

> Les rôles par défaut définis dans Kubernetes sont :

* view : accès en lecture seule, exclut les secrets
* edit : ci-dessus + possibilité de modifier la plupart des ressources, exclut les rôles et les role bindings
* admin : ci-dessus + possibilité de gérer des rôles et des role bindings au niveau d’un espace de noms
* cluster-admin : tout

## Add-ons

> Kubernetes peux être amélioré avec l'utilisation de plugins. Ceux-ci sont classés dans différentes catégories :  
* CRI (Container Runtine Interface) : logiciel permettant de créer, détruire et gérer le cycle de vie d'un conteneur (Docker, Cri-o, rkt, etc).
* CSI (Container Storage Interface) : norme conçue pour unifier l'interface de stockage des orchestrateurs de conteneurs (Ceph, NetApp, GlusterFS, etc).
* CNI (Container Network Interface) : norme conçue pour faciliter la configuration de la mise en réseau des conteneurs lors de leur création ou de leur destruction.
* Cloud-Controller Manager : 
* CRD (Custom Resource Definitions) : 
* API Aggregation : 

# Installation

## Installation de Kubernetes

kubeadm init --apiserver-advertise-address=212.47.238.3 --pod-network-cidr=10.244.0.0/16

## Installation du Dashboard Kubernetes

### Installation du Dashboard

> ```kubectl proxy``` permet de demarrer le serveur web du dashboard et de le lancer sur son poste via le proxy de kubernetes. L'URL d'accès au dashboard est : ```http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/```  

> ```kubectl create serviceaccount dashboard-admin-sa``` permet de créer un compte pour acceder au dashboard.
> ```kubectl create clusterrolebinding dashboard-admin-sa --clusterrole=cluster-admin --serviceaccount=default:dashboard-admin-sa``` permet d'appliquer les droits d'accès aux ressources du cluster.  

> ```kubectl get secrets``` permet d'afficher les comptes présents sur Kubernetes.  

>```kubectl describe secret dashboard-admin-sa-token-tokenID``` permet d'afficher les caractéristiques du compte demandé, dont le token necéssaire à l'accès au dashboard.  

### Installation 
> ```git clone https://github.com/kubernetes-sigs/metrics-server.git``` télécharge depuis Github le logiciel et les scripts d'installation.  

> ```kubectl apply -f deploy/kubernetes/``` lance les scripts YAML pour la création des services/pods/RBAC nécéssaires sur le cluster.