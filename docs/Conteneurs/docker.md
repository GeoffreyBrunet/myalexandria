# Docker

# Types de réseaux
Aucun (none): ajoute le conteneur à une pile réseau spécifique au conteneur, sans aucune connectivité.
Hôte (host): ajoute le conteneur à la pile réseau de la machine hôte, sans isolation.
Pont par défaut (default bridge): Le mode de mise en réseau par défaut. Chaque conteneur peut se connecter entre lui par son adresse IP.
Pont sur mesure (custom bridge): Réseaux de ponts définis par l'utilisateur avec des caractéristiques supplémentaires de flexibilité, d'isolement et de commodité.