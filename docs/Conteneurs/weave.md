# Weave
![logo-weave](https://myalexandriabucket.s3.fr-par.scw.cloud/img/logo-weave.png)
## Weave Firekube

## Weave Flux

## Weave Net
### Fonctionnement
> Weave Net by Weaveworks est une option de réseaux (CNI) pour Kubernetes. Weave crée un réseau maillé superposé entre chacun des nœuds du cluster, permettant un routage flexible entre les participants. Ceci, associé à quelques autres caractéristiques uniques, permet à Weave de router intelligemment dans des situations qui pourraient autrement causer des problèmes.

> Pour créer son réseau, Weave s'appuie sur un composant de routage installé sur chaque hôte du réseau. Ces routeurs échangent ensuite des informations de topologie pour maintenir une vue à jour du paysage réseau disponible. Lorsqu'il cherche à envoyer du trafic à un pod situé sur un autre nœud, le routeur Weave décide automatiquement s'il doit l'envoyer par "chemin de données rapide" ou s'il doit recourir à la méthode de transfert de paquets "sleeve".

> Le chemin de données rapide (Fast datapath) est une approche qui s'appuie sur le module Open vSwitch natif du noyau pour transmettre les paquets au pod approprié sans avoir à entrer et sortir de l'espace utilisateur plusieurs fois. Le routeur Weave met à jour la configuration d'Open vSwitch pour s'assurer que la couche du noyau dispose d'informations précises sur la façon d'acheminer les paquets entrants. En revanche, le mode "sleeve" est disponible en secours lorsque la topologie du réseau n'est pas adaptée au routage rapide des chemins de données. Il s'agit d'un mode d'encapsulation plus lent qui peut acheminer des paquets dans les cas où le chemin de données rapide ne dispose pas des informations de routage ou de la connectivité nécessaires. Au fur et à mesure que le trafic passe par les routeurs, ils apprennent quels pairs sont associés à quelles adresses MAC, ce qui leur permet d'acheminer plus intelligemment avec moins de sauts pour le trafic suivant. Ce même mécanisme permet à chaque nœud de s'autocorriger lorsqu'un changement de réseau modifie les routes disponibles.

![schema-weave-net](https://myalexandriabucket.s3.fr-par.scw.cloud/img/schema-weave-net.png)

> Weave fournit également des capacités de politique de réseau pour votre cluster. Ces fonctionnalités sont automatiquement installées et configurées lorsque vous configurez Weave, de sorte qu'aucune configuration supplémentaire n'est nécessaire, hormis l'ajout de vos règles de réseau. Une chose que Weave fournit que les autres options ne fournissent pas est un cryptage facile pour l'ensemble du réseau. Bien qu'il ajoute un peu de surcharge au réseau, Weave peut être configuré pour crypter automatiquement tout le trafic routé en utilisant le cryptage NaCl pour le trafic de la pochette et, puisqu'il doit crypter le trafic VXLAN dans le noyau, l'ESP IPsec pour le trafic de chemin de données rapide.

> Weave est une excellente option pour ceux qui recherchent un réseau riche en fonctionnalités sans ajouter une grande quantité de complexité ou de gestion. Il est relativement facile à mettre en place, offre de nombreuses fonctionnalités intégrées et configurées automatiquement, et peut assurer le routage dans des scénarios où d'autres solutions pourraient échouer. La topographie en maille impose une limite à la taille du réseau qui peut être raisonnablement accommodée, mais pour la plupart des utilisateurs, cela ne posera pas de problème. En outre, Weave offre un support payant aux organisations qui préfèrent pouvoir contacter quelqu'un pour de l'aide et du dépannage.

### Utilisation
> Pour voir les routes présentes sur le cluster Kubernetes, rentrer la commande suivante sur le node master : "ip route show".
## Weave Scope
> ```kubectl apply -f "https://cloud.weave.works/k8s/scope.yaml?k8s-version=$(kubectl version | base64 | tr -d '\n')"```
> ```kubectl port-forward -n weave "$(kubectl get -n weave pod --selector=weave-scope-component=app -o jsonpath='{.items..metadata.name}')" 4040```