# Objets

## Pod
### Définition
> Un Pod est l’unité d’exécution de base d’une application Kubernetes–l’unité la plus petite et la plus simple dans le modèle d’objets de Kubernetes–que vous créez ou déployez. Un Pod représente des process en cours d’exécution dans votre Cluster.

> Un pode fournit deux types de ressources partagées:
    * réseau
    * stockage

### Commandes relatives aux pods
```kubectl get pods``` permet d'afficher les pods sur l'espace de nom (namespace)
```kubectl apply -f monScript.yml``` applique l'état décrit dans le script YAML.
```kubectl port-forward nomPod portHost:portPod``` permet de mapper le port d'un conteneur sur le port de la machine qui lance la commande. Cette commande permet de faire des tests, elle n'est pas à utiliser en production.
```kubectl describe pod nomPod``` permet de decrire les informations d'un pod.
```kubectl exec -it nomPod sh``` permet de créer un processus dans le conteneur selectionné. "-i" rajoute l'option interactive, pour interragir avec le pods, et "-t" permet d'utiliser un terminal dans le conteneur.
```kuebctl delete -f monScript.yml``` supprime l'état décrit dans le script YAML.
## Service


## Volume


## Namespace

