![Logo Ansible](https://myalexandriabucket.s3.fr-par.scw.cloud/img/logo-ansible.png)

**[Ansible](https://www.ansible.com) est un logiciel d'approvisionnement de serveurs sans agent.** Un seul serveur (nœud controlleur), s'occupe d'appliquer les changements sur les serveurs ciblés.
Des playbooks (fichiers YAML) contiennent les informations à appliquer sur les serveurs distants.

![architecture-ansible](https://myalexandriabucket.s3.fr-par.scw.cloud/img/architecture-ansible.png)

Un rôle contient un ou plusieurs fichiers de configuration (YAML).
Un fichier de configuration contient une ou plusieurs tâches.
Une tâche fait appel à un module.

L'ordonnancement d'un playbook ansible se fait par dossiers donc chacun a un rôle précis :

* Le dossier **Tasks** contient les actions à réaliser.
* Le dossier **Handlers** contient les actions de redémarrage de service à réaliser.
* Le dossier **Defaults** contient les variables d'environnement.
* Le dossier **Meta** contient les dépendances avec un rôle commun.

```ansible-playbook nom_playbook.yaml```